// const router = require('koa-router')();
const User = require('../models/userSchema');
const util = require('../utils/util');
const jwt = require('jsonwebtoken');
const jwt_secret = require('../config/jwt');

module.exports = {
  async login (ctx) {
    try {
      const { username, password } = ctx.request.body;
      const res = await User.findOne({ username, password }, { id: 1, _id: 0 });
      if (res) {
        const data = res._doc;
        const token = jwt.sign(data, jwt_secret.secret, { expiresIn: '1d' });
        data.token = token;
        ctx.body = util.success(data);
      } else {
        ctx.body = util.fail('账号或密码不正确');
      }
    } catch (error) {
      ctx.body = util.fail(error.msg);
    }
  },

  async userInfo (ctx) {
    try {
      let { id } = ctx.request.query;
      id = Number(id);
      const res = await User.findOne(
        { id },
        { avatar: 1, email: 1, id: 1, name: 1, roles: 1, _id: 0 }
      );
      if (res) {
        ctx.body = util.success(res);
      } else {
        ctx.body = util.fail('异常');
      }
    } catch (error) {
      ctx.body = util.fail(error.msg);
    }
  },
  async modifyName (ctx) {
    try {
      const { id, name } = ctx.request.body;
      const res = await User.findOneAndUpdate({ id }, { name });
      if (res) ctx.body = util.success({ info: '修改用户名成功' });
    } catch (error) {
      ctx.body = util.fail(error);
    }

    // ctx.body = util.success(1);
  },
  async modifyPasswd (ctx) {
    try {
      const { id, oldPasswd, passwd } = ctx.request.body;
      console.log(id, oldPasswd, passwd);
      const res = await User.findOneAndUpdate({ id, password: oldPasswd }, { password: passwd });
      if (res) {
        ctx.body = util.success({ info: '修改密码成功' });
      } else {
        ctx.body = util.fail('现有密码不正确，请重新输入');
      }
    } catch (error) {
      ctx.body = util.fail(error);
    }
  }
};
