const Tabs = require('../models/tabsSchema');
const Center = require('../models/centerSchema');

const util = require('../utils/util');

module.exports = {
  async list (ctx) {
    try {
      let { type, keyword } = ctx.request.query;
      const regKeyword = new RegExp(keyword, 'i');
      console.log(keyword);
      let list;
      let total = 0;
      if (type) {
        type = Number(type);
        list = await Center.find({ type, name: { $regex: regKeyword } });
      } else {
        list = await Center.find({ name: { $regex: regKeyword } });
      }
      total = list.length;
      ctx.body = util.success(list, total);
    } catch (error) {
      ctx.body = util.fail(error.msg);
    }
  },
  async tabs (ctx) {
    try {
      const tabs = await Tabs.find({}, { id: 1, name: 1, type: 1, _id: 0 });
      ctx.body = util.success(tabs);
    } catch (error) {
      ctx.body = util.fail(error.msg);
    }
  },
  async operate (ctx) {
    const { id, name, link, branch, type, image, action } = ctx.request.body;
    if (action === 'add') {
      if (!name || !link || !branch || !type) {
        ctx.body = util.fail('参数错误');
        return;
      }
      const res = await Center.findOne({ name });
      if (res) {
        ctx.body = util.fail('已存在同名系统，请重新输入');
      } else {
        try {
          const center = new Center({
            id: new Date().getTime(),
            name,
            link,
            branch,
            type,
            image
          });
          center.save();
          ctx.body = util.success({ info: '创建成功' });
        } catch (error) {
          ctx.body = util.fail('创建失败');
        }
      }
    } else {
      try {
        const res = await Center.findOneAndUpdate(
          { id },
          { name, link, branch, type, image }
        );
        if (res) ctx.body = util.success({ info: '修改成功' });
      } catch (error) {
        ctx.body = util.fail(error.stack, '更新失败');
      }
    }
  },
  async del (ctx) {
    const { id } = ctx.request.body;
    try {
      const res = await Center.findOneAndDelete({ id });
      if (res) {
        ctx.body = util.success({ info: '删除成功' });
      } else {
        ctx.body = util.fail('删除失败');
      }
    } catch (error) {
      ctx.body = util.fail(error.msg);
    }
  },
  async delTabs (ctx) {
    const { id } = ctx.request.body;
    try {
      const res = await Tabs.findOne({ id });
      if (res) {
        const type = res.type;
        const isRecord = await Center.findOne({ type });
        if (isRecord) {
          ctx.body = util.fail('当前归属系统下存在数据，请删除对应数据后再操作');
          return;
        } else {
          await Tabs.findOneAndDelete({ id });
          ctx.body = util.success('删除成功');
          return;
        }
      } else {
        ctx.body = util.fail('删除系统异常');
      }
    } catch (error) {
      ctx.body = util.fail(error.msg);
    }
  },
  async editTabs (ctx) {
    const { id, name, action } = ctx.request.body;
    if (!name) {
      ctx.body = ctx.body = util.fail('请输入正确名称');
      return;
    }
    if (action === 'add') {
      const checkName = await Tabs.findOne({ name });
      if (checkName) {
        ctx.body = ctx.body = util.fail(`${name}已存在`);
        return;
      }
      // 查询最大id，自增
      const maxRes = await Tabs.find({}).sort({ type: -1 }).skip(0).limit(1);
      try {
        let maxType;
        let tmpType;
        if (maxRes.length) {
          maxType = maxRes[0].type;
          tmpType = maxType + 1;
        } else {
          tmpType = 1;
          // ctx.body = ctx.body = util.fail('新增失败');
        }
        const newTab = new Tabs({
          id: new Date().getTime(),
          name,
          type: tmpType
        });
        newTab.save();
        ctx.body = util.success({ info: '新增成功' });
      } catch (error) {}
    } else {
      const checkName = await Tabs.findOne({ name });
      if (checkName) {
        ctx.body = ctx.body = util.fail(`${name}已存在`);
        return;
      }
      try {
        const res = await Tabs.findOneAndUpdate({ id }, { name });
        if (res) ctx.body = util.success({ info: '修改成功' });
      } catch (error) {
        ctx.body = ctx.body = util.fail('修改失败');
      }
    }
  },
  async upload (ctx) {
    const file = ctx.request.files.file;
    ctx.body = { url: `${ctx.origin}/uploads/${file.newFilename}` };
  }
};
