const router = require('koa-router')();
const UserController = require('../controllers/user');

router.prefix('/users');

router.post('/login', UserController.login);

router.get('/userInfo', UserController.userInfo);
router.post('/modifyName', UserController.modifyName);
router.post('/modifyPasswd', UserController.modifyPasswd);
module.exports = router;
