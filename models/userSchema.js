const mongoose = require('mongoose');
const UserSchema = mongoose.Schema({
  id: Number,
  username: String,
  password: String,
  name: String
});

module.exports = mongoose.model('users', UserSchema, 'users');
