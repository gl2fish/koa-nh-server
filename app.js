const Koa = require('koa');
const app = new Koa();
const path = require('path');
const views = require('koa-views');
const json = require('koa-json');
const onerror = require('koa-onerror');
// const bodyparser = require('koa-bodyparser');
const koaBody = require('koa-body');
const cors = require('koa2-cors');

const logger = require('koa-logger');
const koajwt = require('koa-jwt');

const index = require('./routes/index');
const users = require('./routes/users');
const center = require('./routes/center');
const jwt_secret = require('./config/jwt');

// error handler
onerror(app);

require('./config/db');

// middlewares
// app.use(bodyparser({
//   enableTypes: ['json', 'form', 'text']
// }));

app.use(cors());

// app.use(cors({ // 指定一个或多个可以跨域的域名
//   origin: function (ctx) { // 设置允许来自指定域名请求
//     if (ctx.url === '/test') {
//       return '*'; // 允许来自所有域名请求, 这个不管用
//     }
//     return 'http://127.0.0.1:3000'; // 这样就能只允许 http://localhost:8000 这个域名的请求了
//   },
//   maxAge: 10, // 指定本次预检请求的有效期，单位为秒。
//   credentials: true, // 是否允许发送Cookie
//   allowMethods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'], // 设置所允许的HTTP请求方法
//   allowHeaders: ['Content-Type', 'Authorization', 'Accept'], // 设置服务器支持的所有头信息字段
//   exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'] // 设置获取其他自定义字段
// }));
app.use(json());
app.use(logger());
app.use(require('koa-static')(path.join(__dirname, '/public')));
app.use(koaBody({
  // 支持文件格式
  multipart: true,
  formidable: {
    // 上传目录
    uploadDir: path.join(__dirname, 'public/uploads'),
    // 保留文件扩展名
    keepExtensions: true
  }
}));
app.use(views(path.join(__dirname, '/views'), {
  extension: 'pug'
}));

// logger
app.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});

app.use(koajwt({ secret: jwt_secret.secret }).unless({
  path: [/^\/users\/login/,/^\/center\/client\/list/,/^\/center\/client\/tabs/]
}));

// routes
app.use(index.routes(), index.allowedMethods());
app.use(users.routes(), users.allowedMethods());
app.use(center.routes(), center.allowedMethods());

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx);
});

module.exports = app;
