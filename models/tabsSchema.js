const mongoose = require('mongoose');
const tabsSchema = mongoose.Schema({
  id: Number,
  name: String,
  type: Number
});

module.exports = mongoose.model('tabs', tabsSchema, 'tabs');
