const mongoose = require('mongoose');
const centerSchema = mongoose.Schema({
  id: Number,
  name: String,
  link: String,
  branch: String,
  type: Number,
  image: {
    type: String,
    default: ''
  }
});

module.exports = mongoose.model('center', centerSchema, 'center');
