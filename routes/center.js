const router = require('koa-router')();

const CenterController = require('../controllers/center');

router.prefix('/center');

router.get('/list', CenterController.list);
router.get('/client/list', CenterController.list);
router.get('/tabs', CenterController.tabs);
router.get('/client/tabs', CenterController.tabs);
router.post('/operate', CenterController.operate);
router.post('/del', CenterController.del);
router.post('/editTabs', CenterController.editTabs);
router.post('/delTabs', CenterController.delTabs);
router.post('/upload', CenterController.upload);

module.exports = router;
